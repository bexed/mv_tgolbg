/************************************************************************
* SalaryTile class stores all the information of existing salary tiles
*************************************************************************/
function SalaryTile(ID, desc, xPos, yPos, lifePoints, nextTile){
	Tile.call(this,ID,desc,xPos,yPos,lifePoints,nextTile);

	//console.log("Created salary tile " + this.ID + ": " + this.xPos + ", " + this.yPos + ". Desc: " + this.desc);
};
SalaryTile.prototype = Object.create(Tile.prototype);
SalaryTile.prototype.constructor = SalaryTile;


/************************************************************************
* ACCESSORS 
*************************************************************************/


/************************************************************************
* MUTATORS 
*************************************************************************/
SalaryTile.prototype.tileAction = function (player){
	//If the player starts college, pay 60,000
	if(this.ID == 1)
		player.updateMoney(-60000);
	
	player.updateLifePoints(this.lifePoints);
	player.updateMoney(player.getSalary());
};

