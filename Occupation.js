/************************************************************************
* Occupation class stores all the information of existing occupations
*************************************************************************/
var Occupation = function(ID, title, desc, salary, tax, degree){
	//Limit?
	this.ID = ID;
	this.title = title;
	this.desc = desc;
	this.salary = salary;
	this.tax = tax;
	this.requireDegree = degree;

	console.log("Created occupation " + this.ID + ": " + this.title + ", " + this.desc + ". Salary: " + this.salary + ". Tax: " + this.tax);
}

/************************************************************************
* ACCESSORS 
*************************************************************************/
Occupation.prototype.getID = function (){
	return this.ID;
}
Occupation.prototype.getTitle = function (){
	return this.title;
}
Occupation.prototype.getDesc = function (){
	return this.desc;
}
Occupation.prototype.getSalary = function (){
	return this.salary;
}
Occupation.prototype.getTax = function (){
	return this.tax;
}
Occupation.prototype.requiresDegree = function (){
	return this.requireDegree;
}



/************************************************************************
* MUTATORS 
*************************************************************************/
Occupation.prototype.setID = function (newID){
	this.ID = newID;
}
Occupation.prototype.setTitle = function (newTitle){
	this.title = newTitle;
}
Occupation.prototype.setDesc = function (newDesc){
	this.desc = newDesc;
}
Occupation.prototype.setSalary = function (newSalary){
	this.salary = newSalary;
}
Occupation.prototype.setTax = function (newTax){
	this.tax = newTax;
}
Occupation.prototype.setRequirement = function (newReq){
	this.requireDegree = newReq;
}
