/************************************************************************
* EventTile class stores all the information of existing event tiles
*************************************************************************/
function EventTile(ID, desc, xPos, yPos, lifePoints, nextTile, type){
	Tile.call(this,ID,desc,xPos,yPos,lifePoints,nextTile);

	this.type = type;
	//console.log("Created event tile " + this.ID + ": " + this.xPos + ", " + this.yPos + ". Desc: " + this.desc);
};
EventTile.prototype = Object.create(Tile.prototype);
EventTile.prototype.constructor = EventTile;


/************************************************************************
* ACCESSORS 
*************************************************************************/
EventTile.prototype.getType = function (){
	return this.type;
}

/************************************************************************
* MUTATORS 
*************************************************************************/
EventTile.prototype.setType = function (newType){
	this.type = newType;
};
EventTile.prototype.tileAction = function (player){
	
	//If fired/new job
	if(this.type == 0){
		var c = document.getElementById("boardCanvas");
		var ctx = c.getContext("2d");
	
		player.newOccupation();
		
		ctx.font="14px Arial";
		ctx.fillStyle = "#007205";
		ctx.fillText("Your new job is: " + player.getOccupation().getTitle(), 160, 440); 
	}
	//If sue someone
	else if(this.type == 1){
		createChoices(4);
	}
	//If new child
	else if(this.type == 2){
		player.updateChildren(1);
	}
	//If paying taxes
	else if(this.type == 3){
		var payment = player.getOccupation().getTax();
		player.updateMoney(-payment);
		
		//If the player is not an accountant
		if(player.getOccupation().getID() != 6){
			//then check if another player is a accountant,
			for(var i = 0; i < 4; i++){
				//if found then the accountant takes half of the tax amount
				if(PlayerList[i].getOccupation().getID() == 6){
					PlayerList[i].updateMoney(payment/2);
				}
			}
		}	
	}
	//If child expenses
	else if(this.type == 4){
		var payment = player.getChildren()*5000;
		player.updateMoney(-payment);
	}
	
};
