/************************************************************************
* Tile class stores all the information of existing tiles
*************************************************************************/
/*  function Tile(ID, desc, xPos, yPos, nextTile){
	this.ID = ID;
	this.desc = desc;
	this.xPos = xPos;
	this.yPos = yPos;
	this.nextTile = nextTile;

	//console.log("Created tile " + this.ID + ": " + this.xPos + ", " + this.yPos + ". Desc: " + this.desc);
}; */

var Tile = function (ID, desc, xPos, yPos, lifePoints, nextTile){
	this.ID = ID;
	this.desc = desc;
	this.xPos = xPos;
	this.yPos = yPos;
	this.lifePoints = lifePoints;
	this.nextTile = nextTile;
};
 
/************************************************************************
* ACCESSORS 
*************************************************************************/
Tile.prototype.getID = function (){
	return this.ID;
}
Tile.prototype.getDesc = function (){
	return this.desc;
}
Tile.prototype.getXpos = function (){
	return this.xPos;
}
Tile.prototype.getYpos = function (){
	return this.yPos;
}
Tile.prototype.getLifePoints = function (){
	return this.lifePoints;
}
Tile.prototype.getNextTile = function (){
	return TileList[this.nextTile];
}


/************************************************************************
* MUTATORS 
*************************************************************************/
Tile.prototype.setID = function (newID){
	this.ID = newID;
}
Tile.prototype.setDesc = function (newDesc){
	this.desc = newDesc;
}
Tile.prototype.setXpos = function (newX){
	this.xPos = newX;
}
Tile.prototype.setYpos = function (newY){
	this.yPos = newY;
}
Tile.prototype.setLifePoints = function (newPoints){
	this.lifePoints = newPoints;
}
Tile.prototype.setNextTile = function (newNext){
	this.nextTile = newNext;
}
Tile.prototype.tileAction = function (player){
	;
}


