/************************************************************************
* NormalTile class stores all the information of existing normal tiles
*************************************************************************/
function NormalTile(ID, desc, xPos, yPos, lifePoints, nextTile, value){
	Tile.call(this,ID,desc,xPos,yPos,lifePoints,nextTile);
	this.value = value;

	//console.log("Created normal tile " + this.ID + ": " + this.xPos + ", " + this.yPos + ". Desc: " + this.desc + ". Value: " + this.value);
};
NormalTile.prototype = Object.create(Tile.prototype);
NormalTile.prototype.constructor = NormalTile;


/************************************************************************
* ACCESSORS 
*************************************************************************/
Tile.prototype.getValue = function (){
	return this.value;
}


/************************************************************************
* MUTATORS 
*************************************************************************/
Tile.prototype.setValue = function (newValue){
	this.value = newValue;
}
NormalTile.prototype.tileAction = function (player){
	console.debug("NormalTile");
	player.updateLifePoints(this.lifePoints);
	player.updateMoney(this.value);
};

