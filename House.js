/************************************************************************
* House class stores all the information of existing houses
*************************************************************************/
var House = function(ID, title, desc, cost, value){
	this.ID = ID;
	this.title = title;
	this.desc = desc;
	this.cost = cost;
	this.value = value;
	this.occupied = false;

	console.log("Created house " + this.ID + ": " + this.title + ", " + this.desc + ". Cost: " + this.cost + ". Resell Value: " + this.value);
}

/************************************************************************
* ACCESSORS 
*************************************************************************/
House.prototype.getID = function (){
	return this.ID;
}
House.prototype.getTitle = function (){
	return this.title;
}
House.prototype.getDesc = function (){
	return this.desc;
}
House.prototype.getCost = function (){
	return this.cost;
}
House.prototype.getValue = function (){
	return this.value;
}
House.prototype.isOccupied = function (){
	return this.occupied;
}


/************************************************************************
* MUTATORS 
*************************************************************************/
House.prototype.setID = function (newID){
	this.ID = newID;
}
House.prototype.setTitle = function (newTitle){
	this.title = newTitle;
}
House.prototype.setDesc = function (newDesc){
	this.desc = newDesc;
}
House.prototype.setCost = function (newCost){
	this.cost = newCost;
}
House.prototype.setValue = function (newValue){
	this.value = newValue;
}
House.prototype.setOccupant = function (){
	this.occupied = true;
}

