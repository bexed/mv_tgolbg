/************************************************************************
* ForcedTile class stores all the information of existing forced tiles
*************************************************************************/
function ForcedTile(ID, desc, xPos, yPos, lifePoints, nextTile){
	Tile.call(this,ID,desc,xPos,yPos,lifePoints,nextTile);

	//console.log("Created forced tile " + this.ID + ": " + this.xPos + ", " + this.yPos + ". Desc: " + this.desc);
};
ForcedTile.prototype = Object.create(Tile.prototype);
ForcedTile.prototype.constructor = ForcedTile;


/************************************************************************
* ACCESSORS 
*************************************************************************/


/************************************************************************
* MUTATORS 
*************************************************************************/
ForcedTile.prototype.tileAction = function (player){
	var c = document.getElementById("boardCanvas");
	var ctx = c.getContext("2d");
	
	console.debug("ForcedTile");
	player.updateLifePoints(this.lifePoints);
	
	//Graduate college
	if(this.ID == 12 || this.ID == 13){
	
		if(this.ID == 12)
			player.setDegree();
		player.newOccupation();
		
		ctx.font="14px Arial";
		ctx.fillStyle = "#007205";
		ctx.fillText("Your new job is: " + player.getOccupation().getTitle(), 160, 440); 
	}
	//Marriage
	else if(this.ID == 27){
		player.setMarriage();
	}
	//Buy house
	else if(this.ID == 38){
		player.newHouse();
		
		ctx.font="14px Arial";
		ctx.fillStyle = "#007205";
		ctx.fillText("Your new house is: " + player.getHouse().getTitle(), 160, 440); 
	}
	//Baby
	else if(this.ID == 41){
		player.updateChildren(1);
	}
	//Pay raise
	else if(this.ID == 54 || this.ID == 58 || this.ID == 70 || this.ID == 107){
		player.updateSalary(player.getSalary()/10); //Add 10% pay increment
	}
	//Retire
	else if(this.ID == 147){
		
	}
	
};

