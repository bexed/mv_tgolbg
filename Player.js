/************************************************************************
* Player class deals with the player details
*************************************************************************/
var Player = function(money, occupation, salary, loans, curTile){
	this.money = money;
	this.occupation = occupation;
	this.salary = salary;
	this.loans = loans;
	this.house = HouseList[4];
	this.isMarried = false;
	this.children = 0;
	this.investment = 0;
	this.curTile = curTile;
	this.hasDegree = false;
	this.lifePoints = 0;

	console.log("Created a new player.");
}

/************************************************************************
* ACCESSORS 
*************************************************************************/
Player.prototype.getMoney = function (){
	return this.money;
}
Player.prototype.getOccupation = function (){
	return this.occupation;
}
Player.prototype.getSalary = function (){
	return this.salary;
}
Player.prototype.getLoans = function (){
	return this.loans;
}
Player.prototype.getHouse = function (){
	return this.house;
}
Player.prototype.getMarriage = function (){
	return this.isMarried;
}
Player.prototype.getChildren = function (){
	return this.children;
}
Player.prototype.getInvestment = function (){
	return this.investment;
}
Player.prototype.getCurTile = function (){
	return this.curTile;
}
Player.prototype.getDegree = function (){
	return this.hasDegree;
}
Player.prototype.getLifePoints = function (){
	return this.lifePoints;
}


/************************************************************************
* MUTATORS 
*************************************************************************/
Player.prototype.updateMoney = function (amount){
	console.log("Modify money: " + amount);
	this.money += amount;
	
	if(this.money < 0)
		this.updateLoans(20000);
	
}
Player.prototype.setOccupation = function (newOccupation){
	this.occupation = newOccupation;
}
Player.prototype.newOccupation = function (){
	var newOccupation;
	var duplicate = false;
	
	do{
		duplicate = false;
		if(this.hasDegree)
			newOccupation = randomNumber(1,9);
		else
			newOccupation = randomNumber(1,5);
		
		for(var i = 0; i < 4; i++){
			if(PlayerList[i].getOccupation().getID()-1 == newOccupation){
				duplicate = true;
			}
		}
	}while(duplicate);
	
	this.occupation = OccupationList[newOccupation];
	this.salary = this.occupation.getSalary();
}
Player.prototype.setSalary = function (newSalary){
	this.salary = newSalary;
}
Player.prototype.updateSalary = function (amount){
	this.salary += amount;
}
Player.prototype.updateLoans = function (amount){
	console.log("Modify loan: " + amount);
	this.loans += amount;
	
	this.updateMoney(amount);
}
Player.prototype.setHouse = function (newHouse){
	this.house = newHouse;
}
Player.prototype.newHouse = function (){
	var newHouse = 0;
	
	for(var i = 0; i < 4; i++){
		if(!HouseList[i].isOccupied()){
			newHouse = i;
			break;
		}
	}
	
	this.house = HouseList[newHouse];
	this.updateMoney(-this.house.getCost())
	this.house.setOccupant();
}
Player.prototype.setMarriage = function (){
	this.isMarried = true;
}
Player.prototype.updateChildren = function (amount){
	this.children += amount;
}
Player.prototype.setInvestment = function (newInvestment){
	this.investment = newInvestment;
}
Player.prototype.setCurTile = function (newTile){
	this.curTile = newTile;
}
Player.prototype.moveNextTile = function (){
	this.curTile = this.curTile.getNextTile();
}
Player.prototype.setDegree = function (){
	this.hasDegree = true;
}
Player.prototype.updateLifePoints = function (amount){
	this.lifePoints += amount;
}

