/************************************************************************
* BranchTile class stores all the information of existing branch tiles
*************************************************************************/
function BranchTile(ID, desc, xPos, yPos, lifePoints, nextTile, nextTileB){
	Tile.call(this,ID,desc,xPos,yPos,lifePoints,nextTile,nextTileB);
	this.nextTileB = nextTileB;

	//console.log("Created branch tile " + this.ID + ": " + this.xPos + ", " + this.yPos + ". Desc: " + this.desc);
};
BranchTile.prototype = Object.create(Tile.prototype);
BranchTile.prototype.constructor = BranchTile;


/************************************************************************
* ACCESSORS 
*************************************************************************/
BranchTile.prototype.getNextTileB = function (){
	return this.nextTileB;
}


/************************************************************************
* MUTATORS 
*************************************************************************/
BranchTile.prototype.setNextTileB = function (newTile){
	this.nextTileB = newTile;
}
BranchTile.prototype.tileAction = function (player){
	console.debug("BranchTile");
	createChoices(2);
};

